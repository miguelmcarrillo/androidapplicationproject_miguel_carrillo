package com.twopointb.transportproj;

/**
 * Created by miguel on 10/22/15.
 */
public class CarFactory {
    public static Car getCar(int xPos, int yPos, City city, Passenger passenger, CarType carType)
    {
        switch(carType){
            case SEDAN: return new Sedan(xPos,yPos,city,passenger);
            case RACE_CAR: return new RaceCar(xPos,yPos,city,passenger);
            default: return null;
        }
    }
}
