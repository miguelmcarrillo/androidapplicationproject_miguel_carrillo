package com.twopointb.transportproj;

/**
 * Created by miguel on 10/22/15.
 */
public enum CarType {

    SEDAN, RACE_CAR
}
