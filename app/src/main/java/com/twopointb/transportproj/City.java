package com.twopointb.transportproj;


public class City {

    private int __YMax;
    public int getYMax() {
        return __YMax;
    }

    public void setYMax(int value) {
        __YMax = value;
    }

    private int __XMax;
    public int getXMax() {
        return __XMax;
    }

    public void setXMax(int value) {
        __XMax = value;
    }

    public City(int xMax, int yMax) {
        setXMax(xMax);
        setYMax(yMax);
    }

    public Car addCarToCity(int xPos, int yPos, CarType carType) {
        return CarFactory.getCar(xPos, yPos, this, null, carType);
    }

    public Passenger addPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos) {
        return new Passenger(startXPos,startYPos,destXPos,destYPos,this);
    }
}
