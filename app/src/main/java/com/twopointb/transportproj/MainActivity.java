package com.twopointb.transportproj;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.twopointb.transportproj.adapters.MoveAdapter;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.InjectView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends RoboFragmentActivity {

    @InjectView(R.id.move_view)
    private ListView moveView;

    private List<String> moves = new ArrayList<String>();
    private MoveAdapter adapter;
    private int ticks = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeAdapter();
        runSimulation(null);
    }

    public void runSimulation(View view){
        Random rand = new Random();
        int cityLength = 10;
        int cityWidth = 10;
        City myCity = new City(cityLength, cityWidth);
        Car car = myCity.addCarToCity(rand.nextInt(cityLength - 1),rand.nextInt(cityWidth - 1), CarType.RACE_CAR);
        Passenger passenger = myCity.addPassengerToCity(rand.nextInt(cityLength - 1),rand.nextInt(cityWidth - 1),rand.nextInt(cityLength - 1),rand.nextInt(cityWidth - 1));
        clear();
        printInitialData(car, passenger);
        Coordinate dest = new Coordinate(passenger.getCurrentXPos(), passenger.getCurrentYPos());
        car.setDestination(dest);
        while (!passenger.isAtDestination()) {
            tick(car, passenger);
        }

        addMoveToList("Passenger is at destination");
        addMoveToList("Done");
        System.out.println("Done");
    }

    private void clear(){
        adapter.clear();
        ticks = 0;
    }

    private void initializeAdapter() {
        adapter = new MoveAdapter(this, moves);
        moveView.setAdapter(adapter);
    }

    private void addMoveToList(String move) {
        moves.add(move);
    }

    private void addTickToList(String move){
        addMoveToList(++ticks + ". " + move);
    }

    private void printInitialData(Car car, Passenger passenger){
        String msg = "Car started at " + car.getXPos() + ", " + car.getYPos();
        System.out.println(msg);
        addMoveToList(msg);
        msg = "Passenger pickup is at " + passenger.getCurrentXPos() + ", " + passenger.getCurrentYPos();
        addMoveToList(msg);
        msg = "Passenger destination is " + passenger.getDestinationXPos() + ", " + passenger.getDestinationYPos();
        addMoveToList(msg);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Takes one action (move the car one spot or pick up the passenger).
     *
     *  @param car The car to move
     *  @param passenger The passenger to pick up
     */
    private void tick(Car car, Passenger passenger) {

        // Move to passenger
        if (car.getPassenger() != passenger) {
            if (car.getXPos() == passenger.getCurrentXPos() && car.getYPos() == passenger.getCurrentYPos()) {
                passenger.getInCar(car);
                addTickToList("Passenger got in car.");
                return;
            } else if (car.getXPos() < passenger.getCurrentXPos()) {
                car.moveRight();
            } else if (car.getXPos() > passenger.getCurrentXPos()) {
                car.moveLeft();
            } else if (car.getYPos() > passenger.getCurrentYPos()) {
                car.moveDown();
            } else if (car.getYPos() < passenger.getCurrentYPos()) {
                car.moveUp();
            }
        } else {
            // Drive passenger to destination
            if (car.getXPos() < passenger.getDestinationXPos()) {
                car.moveRight();
            } else if (car.getXPos() > passenger.getDestinationXPos()) {
                car.moveLeft();
            } else if (car.getYPos() > passenger.getDestinationYPos()) {
                car.moveDown();
            } else if (car.getYPos() < passenger.getDestinationYPos()) {
                car.moveUp();
            }
        }
        addTickToList(car.printPosition());
    }


}
