package com.twopointb.transportproj;

/**
 * Created by miguel on 10/22/15.
 */
public class RaceCar extends Car {

    public RaceCar(int xPos, int yPos, City city, Passenger passenger) {
        super(xPos, yPos, city, passenger);
    }

    public void moveUp() {

        int maxMoves = getCity().getYMax() - getYPos();
        if (maxMoves <= 0) {
            return;
        }

        maxMoves = Math.abs(getDestination().getY() - getYPos());
        if (maxMoves == 0) {
            return;
        }

        if (maxMoves >= 2) {
            setYPos(getYPos() + 2);
        } else {
            setYPos(getYPos() + 1);
        }
        printPosition();

    }

    public void moveDown() {
        if (getYPos() <= 0) {
            return;
        }

        int maxMoves = Math.abs(getDestination().getY() - getYPos());
        if (maxMoves == 0) {
            return;
        }

        if (maxMoves >= 2) {
            setYPos(getYPos() - 2);
        } else {
            setYPos(getYPos() - 1);
        }
        printPosition();

    }

    public void moveRight() {

        int maxMoves = getCity().getXMax() - getXPos();
        if (maxMoves <= 0) {
            return;
        }

        maxMoves = Math.abs(getDestination().getX() - getXPos());
        if (maxMoves == 0) {
            return;
        }

        if (maxMoves >= 2) {
            setXPos(getXPos() + 2);
        } else {
            setXPos(getXPos() + 1);
        }
        printPosition();

    }

    public void moveLeft() {

        if (getXPos() <= 0) {
            return;
        }

        int maxMoves = Math.abs(getDestination().getX() - getXPos());
        if (maxMoves == 0) {
            return;
        }

        if (maxMoves >= 2) {
            setXPos(getXPos() - 2);
        } else {
            setXPos(getXPos() - 1);
        }
        printPosition();

    }

    protected String printPosition() {
        String str = String.format("Race car moved to x - %d y - %d", new Object[] {getXPos(),getYPos()});
        System.out.println(str);
        return str;
    }
}