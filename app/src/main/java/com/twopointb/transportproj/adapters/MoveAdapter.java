package com.twopointb.transportproj.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.twopointb.transportproj.R;

import java.util.List;

/**
 * Created by miguel on 10/22/15.
 */
public class MoveAdapter extends ArrayAdapter<String> {

    private final List<String> values;

    public MoveAdapter(Context context, List<String> values) {
        super(context, R.layout.move_row, values);
        this.values = values;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater li = LayoutInflater.from(getContext());
            convertView = li.inflate(R.layout.move_row, null);

            holder = new ViewHolder();
            holder.msgView = (TextView) convertView.findViewById(R.id.msgView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String move = getItem(position);
        if (move != null) {
            String msg = values.get(position);
            holder.msgView.setText("" + msg);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView msgView;
    }
}